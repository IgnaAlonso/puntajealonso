#pragma once

#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <time.h>
#include <string.h>

using namespace std;


class Puntajes
{
public:
	Puntajes();
	void cargarPuntajes(int puntaje);
	void mostrarPuntajeMaximo();
	void mostrarPuntaje(int puntaje);
	void mostrarTablaPuntaje();
	

private:
	int puntajes[5]{ 0,0,0,0,0 };
};
