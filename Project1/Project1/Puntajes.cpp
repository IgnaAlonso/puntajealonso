#include "Puntajes.h"

Puntajes::Puntajes()
{
}

void Puntajes::cargarPuntajes(int puntaje)
{
	int aux = 0;

	for (int i = 0; i <= 5; i++)
	{
		if (puntaje > puntajes[i])
		{
			aux = puntajes[i];
			puntajes[i] = puntaje;
			puntaje = aux;
		}
	}
}

void Puntajes::mostrarPuntajeMaximo()
{
	cout << "El puntaje maximo es " << puntajes[0] << endl;
}

void Puntajes::mostrarPuntaje(int puntaje)
{
	cout << "Tu puntaje fue: " << puntaje << endl;
}

void Puntajes::mostrarTablaPuntaje()
{

	cout << "Lista de puntajes mas altos: " << endl;

	for (int i = 0; i <= 5; i++)
	{
		cout << i + 1 << ". " << puntajes[i] << endl;
	}
}