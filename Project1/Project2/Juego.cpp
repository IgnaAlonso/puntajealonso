#include "Puntajes.h"


int main(void)
{
	Puntajes puntajes;

	bool terminar = false;

	int puntaje = 0;

	int random;

	char juego;
	char terminarJuego;

	srand(time(NULL));

	random = rand() % 50;

	cout << "Adivina la letra (Ingresa cualquier letra en minuscula)" << endl;
	cin >> juego;

	while (!terminar)
	{
		if (juego == 'y')
		{
			cout << "Ganaste !" << endl;
			puntajes.mostrarPuntaje(puntaje);
			cout << "Volver a Jugar s (si) o n (no)" << endl;
			cin >> terminarJuego;

			if (terminarJuego == 'n')
			{
				terminar = true;
			}

			if (terminarJuego == 's')
			{
				juego = 'f';
			}

			puntajes.cargarPuntajes(puntaje);
		}
		else
		{
			puntaje++;
			cout << "Volver a intentar" << endl;
			cin >> juego;
		}
	}

	puntajes.mostrarPuntaje(puntaje);

	puntajes.mostrarPuntajeMaximo();
	puntajes.mostrarTablaPuntaje();

}